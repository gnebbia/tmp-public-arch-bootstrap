#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of samba software"
###############################################################################

list=(
samba
gvfs-smb
)

count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

echo "################################################################"
echo "Getting the ArcoLinux Samba config"
echo "################################################################"
echo ""

cp /etc/samba/smb.conf.arcolinux /etc/samba/smb.conf

echo "################################################################"
echo "Give your username for samba"
echo "################################################################"
echo ""

read -p "What is your login? It will be used to add this user to smb: " choice
smbpasswd -a $choice

echo "################################################################"
echo "Enabling services"
echo "################################################################"
echo ""

# systemctl enable smb.service
# systemctl enable nmb.service

echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo ""
