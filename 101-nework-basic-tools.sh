#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of network basic tools"
###############################################################################

list=(
networkmanager
nm-connection-editor
network-manager-applet
net-tools
wget
openssh
dhcpcd
inetutils
)


count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    echo "Installing package nr.  "$count " " $name;
    pkg_install $name
done


echo "################################################################"
echo "Enabling NetworkManager"
echo "################################################################"
systemctl enable NetworkManager

echo "################################################################"
echo "Reboot your system"
echo "################################################################"
