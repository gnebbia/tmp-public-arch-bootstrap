#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of the development packages"
###############################################################################

list=(
base-devel
openssl
zlib
emacs
strace
ltrace
gdb
)

count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

tput setaf 11;
echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo;tput sgr0
