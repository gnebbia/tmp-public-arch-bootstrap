# gn bootstrap scripts for arch gnu/linux

My Arch Bootstrapping scripts, heavily inspired by the scripts contained
in ArcoLinuxD.

These scripts can be run after we have chrooted into the arch installated
environment and have installed git.

Configuration can be managed through the `config.sh` file.

# Installation

## Put Installation ISO on USB Drive
dd bs=4M if=path/to/archlinux.iso of=/dev/sdx status=progress oflag=sync

## On target machine
NOTE: Alt+Arrow can switch to another console, e.g., to view the installation guide with lynx

    ls /usr/share/kbd/keymaps/**/*.map.gz | less ;; skip this if you are on a US international keyboard check available keyboard layouts
    loadkeys <keyboard-layout> ;; skip this if you are on a US international keyboard

    ls /sys/firmware/efi/efivars  ;; verify if this exists, it means we booted through efi

    ip link ;; check network cards

If we are on ethernet cable we should already be able to ping...

To connect to wifi do:
iwctl ;;
    device list
    station wlp1s0 scan
    station wlp1s0 get-networks
    station wlp1s0 connect myWifeHighNetwork

ping archlinux.org  ;; check connectivity

timedatectl set-ntp true ;; synchronize clock

fdisk -l ;; check disk names

fdisk /dev/nvme0n1 ;; partition nvme0n1 disk
    ;; MINIMUM PARTITIONING SCHEME WITH UEFI COMPUTERS
    make 1st partition at least 512M of type EFI System
    make 2nd partition linux swap
    make 3rd partition the rest of the disk Linux Filesystem (should be the default)

fdisk -l ;; check partition names
mkfs.fat -F32 /dev/efi_partition
mkfs.ext4 /dev/root_partition   ;; e.g., mkfs.ext4 /dev/nvme0n1p3
mkswap /dev/swap_partition
swapon /dev/swap_partition

Create directories for partitions and mount them:
mount /dev/root_partition /mnt
mkdir /mnt/boot
mount /dev/efi_or_boot_partition /mnt/boot


pacstrap /mnt base base-devel git linux linux-firmware vim

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt

ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime
At this point I use some personal scripts to bootstrap my system (arch-bootstrap).
