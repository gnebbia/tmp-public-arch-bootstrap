#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh

func_category() {
    tput setaf 5;
    echo "################################################################"
    echo "Installing software for category " $1
    echo "################################################################"
    echo;tput sgr0
}

###############################################################################

func_category Conky

list=(
conky-lua-archers
yad
libpulse
)

count=0
for name in "${list[@]}" ; do
	count=$[count+1]
	tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
	pkg_install $name
done

echo "################################################################"
echo "Copying all files and folders from /etc/skel to ~"
echo "################################################################"
echo ""

cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S)
cp -arf /etc/skel/. ~
echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo ""
