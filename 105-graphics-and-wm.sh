#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh
source ./config.sh


###############################################################################
echo "Installation of graphics and window manager with Intel and Nvidia drivers"
###############################################################################

list=(
xf86-video-intel
xorg
xorg-xinit
xclip
dunst
dmenu
rofi
picom
i3
nitrogen
arandr
autorandr
w3m
lxappearance
# xf86-video-nouveau
)


count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    echo "Installing package nr.  "$count " " $name;
    pkg_install $name
done

cp other_configs/xinitrc "$NEWUSERHOME"/.xinitrc
echo 'if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi' >> $NEWUSERHOME/.bashrc
chown -R $USERNAME.$USERNAME $NEWUSERHOME/

# Enable Tap to click
[ ! -f /etc/X11/xorg.conf.d/40-libinput.conf ] && printf 'Section "InputClass"
        Identifier "libinput touchpad catchall"
        MatchIsTouchpad "on"
        MatchDevicePath "/dev/input/event*"
        Driver "libinput"
        # Enable left mouse button by tapping
        Option "Tapping" "on"
EndSection' > /etc/X11/xorg.conf.d/40-libinput.conf

mkdir -p /usr/share/backgrounds/personal
mv wallpapers/ /usr/share/backgrounds/personal

echo "################################################################"
echo "Reboot your system"
echo "################################################################"


