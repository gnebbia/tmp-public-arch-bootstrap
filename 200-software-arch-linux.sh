#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh




func_category() {
    echo "################################################################"
    echo "Installing software for category " $1
    echo "################################################################"
    echo ""
}

###############################################################################

func_category Accessories

list=(
redshift
vifm
ranger
jq
proxychains
gparted
keepassxc
youtube-dl
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

func_category Development

list=(
downgrade
inxi
pamac-aur
imagemagick
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

func_category Graphics

list=(
gimp
inkscape
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

func_category Internet

list=(
firefox
qbittorrent
tor
irssi
lynx
amfora
hexchat
telegram-desktop
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

func_category Multimedia

list=(
mpc
mpd
mpv
cmus
vlc
ffmpeg
peek
scrot
sxiv
viewnior
simplescreenrecorder
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

func_category Office

list=(
qpdfview
xpdf
zathura
zathura-pdf-mupdf
poppler
mediainfo
libreoffice
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done


func_category Unpack

list=(
p7zip
unrar
zip
unzip
)

count=0
for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

echo "################################################################"
echo "Software has been installed"
echo "################################################################"
