#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of the core software"
###############################################################################

list=(
neovim
htop
alacritty
libinput
xf86-input-synaptics
man-db
detox
pkgfile
tmux
bat
ncdu
bc
exfat-utils
ntfs-3g
dosfstools
udiskie
)


count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    echo "Installing package nr.  "$count " " $name;
    pkg_install $name
done


echo "################################################################"
echo "Reboot your system"
echo "################################################################"
