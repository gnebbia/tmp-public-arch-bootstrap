#!/bin/sh

# Execute this when you have chrooted successfully


source ./config.sh


echo "######################################################"
echo "Installing git"
echo "######################################################"
pacman -S --noconfirm --needed  git doas sudo


echo "######################################################"
echo "Setting Timezone"
echo "######################################################"
ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime


echo "######################################################"
echo "Synchronizing Hardware Clock"
echo "######################################################"
hwclock --systohc

echo "######################################################"
echo "Generating Locales"
echo "######################################################"
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "######################################################"
echo "Setting Hostname"
echo "######################################################"
echo "$HOSTNAME" > /etc/hostname

echo "######################################################"
echo "Configuring the hosts file"
echo "######################################################"
cat << EOF > /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   $HOSTNAME.localdomain  $HOSTNAME
EOF

echo "######################################################"
echo "Setting Root Password"
echo "######################################################"
passwd

echo "######################################################"
echo "Installing GRUB"
echo "######################################################"
pacman -S --noconfirm --needed grub 
if [[ "$FIRMWARE" = "BIOS" ]]; then
    grub-install --target=i386-pc $MAIN_DISK
else
    pacman -S --noconfirm --needed os-prober efibootmgr 
    grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
fi

grub-mkconfig -o /boot/grub/grub.cfg

echo "######################################################"
echo "Adding a non-privileged user"
echo "######################################################"
useradd -m -G wheel,video,audio,lp,network,power -s "$USER_SHELL" "$USERNAME"


echo "######################################################"
echo "Setting non-privileged user password"
echo "######################################################"
passwd $USERNAME

chown -R $USERNAME.$USERNAME $NEWUSERHOME/



echo "permit $USERNAME as root" > /etc/doas.conf
