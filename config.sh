# Change settings according to your preferences:
# - USERNAME: name of the main non-root user
# - NEWUSERHOME: do not touch this variable
# - HOSTNAME: the name of the host
# - FIRMWARE: either "UEFI" or "BIOS"
# - USER_SHELL: whatever shell you like
# - MAIN_DISK: disk where arch will be installed
USERNAME="user"
NEWUSERHOME=/home/$USERNAME
HOSTNAME="archost"
FIRMWARE="UEFI"
USER_SHELL="/bin/bash"
MAIN_DISK="/dev/nvme0n1"
