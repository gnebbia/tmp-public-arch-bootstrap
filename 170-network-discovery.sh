#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of network discovery software"
###############################################################################

list=(
avahi
nss-mdns
gvfs-smb
)

count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

echo "################################################################"
echo "Change nsswitch.conf for access to nas servers"
echo "################################################################"
echo ""


#first part
sed -i 's/files mymachines myhostname/files mymachines/g' /etc/nsswitch.conf
#last part
sed -i 's/\[\!UNAVAIL=return\] dns/\[\!UNAVAIL=return\] mdns dns wins myhostname/g' /etc/nsswitch.conf

echo "################################################################"
echo "Enabling services"
echo "################################################################"
echo ""

systemctl enable avahi-daemon.service

echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo ""
