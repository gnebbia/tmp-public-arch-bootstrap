#!/usr/bin/env  bash


./000-init.sh && \
./100-core.sh && \
./101-nework-basic-tools.sh && \
./105-graphics-and-wm.sh && \
./110-development-software.sh && \
./120-sound.sh && \
./130-bluetooth.sh && \
./140-printers.sh && \
./150-samba.sh && \
./160-laptop.sh && \
./170-network-discovery.sh && \
./200-software-arch-linux.sh && \
./210-additional-network-tools.sh && \
./300-aur-helper.sh && \
./400-pentesting.sh
