#!/bin/bash
#set -e
###############################################################################
# Author        : Giuseppe Nebbione
# Website       : gnebbia.ml
###############################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
###############################################################################


source ./utilities.sh



###############################################################################
echo "Installation of bluetooth software"
###############################################################################

list=(
pulseaudio-bluetooth
bluez
blueman
bluez-libs
bluez-utils
blueberry
)

count=0

for name in "${list[@]}" ; do
    count=$[count+1]
    tput setaf 3;echo "Installing package nr.  "$count " " $name;tput sgr0;
    pkg_install $name
done

###############################################################################

echo "################################################################"
echo "Enabling services"
echo "################################################################"
echo ""
 
systemctl enable bluetooth.service
sudo sed -i 's/'#AutoEnable=false'/'AutoEnable=true'/g' /etc/bluetooth/main.conf


echo "################################################################"
echo "Software has been installed"
echo "################################################################"
echo ""
